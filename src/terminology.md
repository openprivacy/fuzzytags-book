# Terminology

Throughout this book, the fuzzytags API and documentation we use the following terms, some of which
deviate and/or extend definitions from the original paper.
## **Tag**

A probabilistic structure that can be attached to a message in order to identify the recipient. The focus of the
fuzzytags system.
 
##  **Root Secret**  

A privately generated set of secret scalars. Randomly generated.

Not to be confused with "secret/private key" in larger system integrations.

## **Tagging Key** 

A publicly distributed set of group elements used to construct a tag for a given party. Derived from the Root Secret.

Not to be confused with "public key" in larger system integrations.


## **Detection Key** 

A semi-public subvector of the Root Secret that is provided to an adversarial server in order to outsource identification
of tags (with some pre-determined false positive rate).

Not to be confused with "verification" key in larger system integrations.