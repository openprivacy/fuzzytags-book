# Simulating fuzzytags

We also provide simulation software to experiment with various deployment of
fuzzytags given certain system parameters like the number of parties, a model of
message sending, and the rate of entangled tags.

![](./simulation.png)
