# Summary

- [Introduction](introduction.md)
- [Terminology](./terminology.md)
- [Risk Model](./risk-model.md)
    - [Intersection Attacks](./intersection-attacks.md)
    - [Statistical Attacks](./statistical-attacks.md)
- [Deploying Fuzzytags Securely](./deploying-fuzzytags.md)
- [Entangled Tags](./entangled.md)
    - [Entangling Strategies](./entangling-strategies.md)
- [Simulating fuzzytags](./simulations.md)
    - [Email EU Core Dataset Simulations](./simulation-eu-core-email.md)
    - [College IM Dataset Simulations](./simulation-college-im.md)