# Risk Model

In this section we will document the risk model and attacks that will likely impact practical deployments 
of fuzzytags.

We assume a set of $n$ parties sending tags and messages through an untrusted server.

For some of the analysis in this notebook we will assume that the server has knowledge of the sender of each tag. 

While this is not desirable in real-world deployments, being able to match tags to senders does allow us to derive 
bounds on worst case metadata leakage. 