# Entangling Strategies

In this section we will document a number of different strategies that applications might consider 
when implementing an entangling scheme. Where possible we will provide analysis and simulations to assess privacy
impact of each one.

## No Entangling

For completeness, let's consider no entangling. In this case each tag has a single recipient. We have shown through
simulations that fuzzytags, in aggregate, leak information to the server. Being able to attribute a fuzzytag to a 
receiver with confidence reduces the false positive rate on the derived graph. 

On the other hand, all entangling strategies require access to a global database of tagging keys in order to be
effective - this has concrete privacy and security concerns in-and-of itself.

## Random Entangling

One of the simplest schemes involves every sender selecting a single random entangled recipient. We have simulated
this strategy against real world datasets and found that it does little to prevent the server from deriving the underlying
social graph - only marginally increasing the false positive rate of connections on the derived graph.

To understand why, consider that entangling tags in this way doesn't create a signal intended to corrupt the 
derived graph, but instead adds noise to the number of received tags for each detection key. As the number of legitimate
tags received increases, they create a signal that stands above the noise floor of the generated tags.

## Deniable Entangling

As outlined in the last section, one of the major properties of entangled tags is deniable sending i.e. they make it
possible to legitimately send a message to multiple parties.

One possible deployment approach is to have a one or more known popular services using fuzzytags to receive messages,
and for all parties to entangle their messages to both the intended recipient *and* a popular service.

This approach provides two distinct benefits:

1. Only the tagging keys of popular services need to be known - these can be public and heavily popularized, discounting most
security and privacy concerns with maintaining a database of keys to entangle to.
2. Every tag in this scheme is individually deniability, increasing the anonymity set of all received tags.
3. Such services provide a bootstrapping mechanism for a network based on fuzzytags

The one major drawback of this approach centres around the addition of a number of select service providers into the 
anonymity model resulting in a psuedo-non-collusion assumption, in addition to raising the spectre of compulsion attacks.

Both of these drawbacks can be mitigated through diversification of the entanglement pool - at it must be pointed out
that unlike many non-collusion assumptions there is no direct systemic relationships between the adversarial server
providing fuzzytags and services built on top of such a service i.e. anyone can start providing a service and anyone
can start entangling to a service without these being core features to the anonymity system.

It must be stated that such an entangling scheme does not remove the risk that aggregate tags will reveal the 
underlying social network  (as the signal is still there), it simply invokes a deniability argument for the relationship.
This may be sufficient, but is not always going to be so.

As such any simulation of this approach will likely be identical to one for not-entangling at all - with the false
positive result replaced with a deniability disclaimer.
